const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => m && m.default || m


exports.components = {
  "component---cache-dev-404-page-js": hot(preferDefault(require("/Users/sbacha/Desktop/gitlab-ft/ft-gatsby/.cache/dev-404-page.js"))),
  "component---src-pages-404-js": hot(preferDefault(require("/Users/sbacha/Desktop/gitlab-ft/ft-gatsby/src/pages/404.js"))),
  "component---src-pages-auth-0-callback-js": hot(preferDefault(require("/Users/sbacha/Desktop/gitlab-ft/ft-gatsby/src/pages/auth0_callback.js"))),
  "component---src-pages-components-freight-chart-index-js": hot(preferDefault(require("/Users/sbacha/Desktop/gitlab-ft/ft-gatsby/src/pages/components/FreightChart/index.js"))),
  "component---src-pages-freight-index-mdx": hot(preferDefault(require("/Users/sbacha/Desktop/gitlab-ft/ft-gatsby/src/pages/freight/index.mdx"))),
  "component---src-pages-freight-table-mdx": hot(preferDefault(require("/Users/sbacha/Desktop/gitlab-ft/ft-gatsby/src/pages/freight/table.mdx"))),
  "component---src-pages-index-mdx": hot(preferDefault(require("/Users/sbacha/Desktop/gitlab-ft/ft-gatsby/src/pages/index.mdx"))),
  "component---src-pages-table-mdx": hot(preferDefault(require("/Users/sbacha/Desktop/gitlab-ft/ft-gatsby/src/pages/table.mdx")))
}

