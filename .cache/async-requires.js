// prefer default export if available
const preferDefault = m => m && m.default || m

exports.components = {
  "component---cache-dev-404-page-js": () => import("./dev-404-page.js" /* webpackChunkName: "component---cache-dev-404-page-js" */),
  "component---src-pages-404-js": () => import("./../src/pages/404.js" /* webpackChunkName: "component---src-pages-404-js" */),
  "component---src-pages-auth-0-callback-js": () => import("./../src/pages/auth0_callback.js" /* webpackChunkName: "component---src-pages-auth-0-callback-js" */),
  "component---src-pages-components-freight-chart-index-js": () => import("./../src/pages/components/FreightChart/index.js" /* webpackChunkName: "component---src-pages-components-freight-chart-index-js" */),
  "component---src-pages-freight-index-mdx": () => import("./../src/pages/freight/index.mdx" /* webpackChunkName: "component---src-pages-freight-index-mdx" */),
  "component---src-pages-freight-table-mdx": () => import("./../src/pages/freight/table.mdx" /* webpackChunkName: "component---src-pages-freight-table-mdx" */),
  "component---src-pages-index-mdx": () => import("./../src/pages/index.mdx" /* webpackChunkName: "component---src-pages-index-mdx" */),
  "component---src-pages-table-mdx": () => import("./../src/pages/table.mdx" /* webpackChunkName: "component---src-pages-table-mdx" */)
}

