import HomepageTemplate from "gatsby-theme-carbon/src/templates/Homepage";
import { ArticleContainer, CustomArticleCard, BrandsList, SnapContent, SnapCard, SignUpContainer, BannerContainer, RequestDemoContent, TextImage } from "gatsby-theme-carbon/src/templates/HomepageComponents";
import { Button } from "carbon-components-react";
import * as React from 'react';
export default {
  HomepageTemplate,
  ArticleContainer,
  CustomArticleCard,
  BrandsList,
  SnapContent,
  SnapCard,
  SignUpContainer,
  BannerContainer,
  RequestDemoContent,
  TextImage,
  Button,
  React
};