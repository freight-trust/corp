import HomepageTemplate from "gatsby-theme-carbon/src/templates/Homepage";
import { ArticleContainer, BrandsList, SnapContent, SnapCard, SignUpContainer, BannerContainer, RequestDemoContent } from "gatsby-theme-carbon/src/templates/HomepageComponents";
import { Button } from "carbon-components-react";
import * as React from 'react';
export default {
  HomepageTemplate,
  ArticleContainer,
  BrandsList,
  SnapContent,
  SnapCard,
  SignUpContainer,
  BannerContainer,
  RequestDemoContent,
  Button,
  React
};